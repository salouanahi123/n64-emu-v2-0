# N64 Online

### Latest version: v2.0

These cores are included pre-installed with the repository:

* Mupen64Plus-Next (Nintendo 64)
* ParaLLEl N64 (Nintendo 64)

## Table of contents
* [Top](#n64-online)
* [Table of contents](#table-of-contents)
* [Features](#features)
* [Notes](#notes)
* [Credits](#credits)

## Features

* Users can select from every game released on N64 console (including Japan Exclusives).
* Users can restart emulation within game via selecting the 'RESTART' feature.
* Users can pause emulation within game via selecting the 'PAUSE' feature.
* Users can save their current progress as an SRAM file within game via selecting the 'SAVE STATE' feature or pressing Shift + F2 on keyboard.
* Users can load their former progress within game via selecting the 'SAVE STATE' feature or pressing Shift + F4 on keyboard, then navigating their Google Drive for an SRAM from the game being played.
* Users can record footage from within game via selecting the 'START/STOP SCREEN RECORDING' feature; stopped recordings will automatically be saved to their Google Drive as a WEBM file.
* Users can play their emulation of choice online and with friends from within game via selecting the 'NETPLAY' feature.
* Users can configure keybinds within game via selecting the 'CONTROL SETTINGS' feature.
* Users can import and use cheat codes from within game via selecting the 'CHEAT CODES' feature.
* Users can choose between the contents of a curated collection of shaders that apply visual effects to their emulation from within game via opening 'SEETINGS' and selecting the 'SHADERS' feature.
* Users can choose between 4:3 or 16:9 Aspect Ratio from within game via opening 'SETTINGS' and selecting the 'ASPECT RATIO' feature.
* Users can configure resolution of 4:3 Aspect Ratio from within game via opening 'SETTINGS' and selecting the '4:3 RESOLUTION' feature.
* Users can configure resolution of 16:9 Aspect Ratio from within game via opening 'SETTINGS' and selecting the '16:9 RESOLUTION' feature.

## Notes

libretro emscripten support tracker: [spreadsheet](https://docs.google.com/spreadsheets/d/13Lse1ipcUIBb8drVyIl6NKNliW5fFXyfkpJnxb2h1SE)

* Mupen64Plus-Next would sometimes encounter a stack overflow error on some games. The stack size has been increased to mitigate this, but it can still happen on some games if left running for long enough. The root cause of the stack overflow has not yet been found. It is still recommended to use this core rather than ParaLLEl N64, because this core has the better renderer. If the issue is too much of a problem, use ParaLLEl N64 instead.
* Stella (latest) immediately exits on content load for some reason. Stella 2014 is used instead.

## Acknowledgements

Credit to all these incredible people:

ethanaobrian: 205 commits
allancoding: 41 commits
n-at: 34 commits
ElectronicsArchiver: 6 commits
incredibleIdea: 3 commits
E-Sh4rk: 2 commits
Grey41: 2 commits
andrigamerita: 1 commit
oyepriyansh: 1 commit
debuggerx01: 1 commit
michael-j-green: 1 commit
eric183: 1 commit
Protektor-Desura: 1 commit
cheesykyle: 1 commit
imneckro: 1 commit
